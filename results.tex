\section{Performance Results}
\label{sec:cublas_comparison}
This section shows the performance results of the proposed MAGMA kernels against the equivalent implementations 
by the vendor library (cuBLAS).

\subsection{Experimental Setup}
The performance tests are conducted on a system equipped with a two-socket CPU 
(Intel Xeon E5-2650 v3 @ $2.30$GHz), with $10$ cores per socket, and a Tesla V100-PCIe GPU. 
The GPU has $16$GB of memory, and $80$ streaming multiprocessors, which are clocked at $1.38$GHz. 
We use CUDA Toolkit $10.1$ for the compilation of the MAGMA kernels, as well as for testing cuBLAS. 
The tests are done for relatively small square sizes and for relatively small rank-16 updates. 

\subsection{Performance of the Batched-HGEMM Kernel}
Figure~\ref{fig:hgemm_batched} shows the performance of the tuned MAGMA kernel against cuBLAS for the 
batched HGEMM operation. Performance speedups are observed for the MAGMA kernel for sizes up to $100$ on square sizes 
and for sizes up to $128$ for the rank-16 updates. This behavior shows the importance of auto-tuning. For small 
problem sizes, we notice that the performance of a given kernel is sensitive to tuning parameters in the sense that more kernel 
instances are required for relatively small problems. This is unlike the situation for larger sizes, where usually one or two kernel 
instances can deliver the best performance. 
\begin{figure}[!htb]
\centering
\subfloat[Square sizes]{
\includegraphics[width=0.8\linewidth]{./results/final_perf/V100-PCIe/hgemm_batched_sq_v100PCIe.pdf}
} \\
\subfloat[Rank-16 updates]{
\includegraphics[width=0.8\linewidth]{./results/final_perf/V100-PCIe/hgemm_batched_k16_v100PCIe.pdf}
}
\caption{Performance of the batched HGEMM kernel against cuBLAS. Results are for 
square sizes up to $128$, with \tt{batchCount} = $1000$, on a Tesla V100-PCIe GPU.}
\label{fig:hgemm_batched}
\end{figure}

Figure~\ref{fig:speedup_hgemm_batched_v100PCIe} summarizes the performance speedup for every size between $10$ and $128$. On average, 
the speedup numbers range between $1.5\times$ and $2.8\times$, except for few spikes or drops. The spikes/drops in 
speedup are mainly due to the cuBLAS performance behavior, which seem to have periodic drops for some sizes $\leq 64$, and 
some other performance spikes after that.  
\begin{figure}[!htb]
\includegraphics[width=0.8\linewidth]{./results/final_perf/V100-PCIe/speedup_hgemm_batched_v100PCIe.pdf}
\caption{Performance speedup of the batched HGEMM kernel against cuBLAS. Results are for sizes up 
to $128$, with \tt{batchCount} = $1000$, on a Tesla V100-PCIe GPU.}
\label{fig:speedup_hgemm_batched_v100PCIe}
\end{figure}

Recall that the cuBLAS kernel is written in a low-level language to utilize some optimization techniques 
that are not available in CUDA C or PTX instructions. And so, its asymptotic performance is faster than 
MAGMA by factors greater than $2\times$ for large matrices. However, the significant cuBLAS advantage is observable only 
for sizes that are multiples of $8$. As an example, for a batch of $100$ square problems of size $2000$, the cuBLAS kernel 
is $2.3\times$ faster than MAGMA ($68.8$ teraFLOP/s for cuBLAS vs. $29.5$ teraFLOP/s for MAGMA). However, the same batch for 
sizes of $2100\times 2100$ sees a significant drop for cuBLAS vs. a slight one for MAGMA. In fact, MAGMA has a slight 
$3\%$ advantage in this case ($26.5$ teraFLOP/s for cuBLAS vs. $27.4$ teraFLOP/s for MAGMA). In general, large sizes that are not 
multiples of $8$ witness competitive performance numbers from both libraries. 
\begin{figure}[!htb]
\centering
\subfloat[Square sizes]{
\includegraphics[width=0.8\linewidth]{./results/final_perf/V100-PCIe/hcgemm_batched_sq_v100PCIe.pdf}
} \\
\subfloat[Rank-16 updates]{
\includegraphics[width=0.8\linewidth]{./results/final_perf/V100-PCIe/hcgemm_batched_k16_v100PCIe.pdf}
}
\caption{Performance of the batched HCGEMM kernel (interleaved layout) against cuBLAS (planar layout). 
Results are for square sizes up to $256$, with \tt{batchCount} = $1000$, on a Tesla V100-PCIe GPU.}
\label{fig:hcgemm_batched}
\end{figure}

\subsection{Performance of the Batched-HCGEMM Kernel}
Figure~\ref{fig:hcgemm_batched} shows the performance of the batched HCGEMM kernels, 
\myc{while Figure~\ref{fig:speedup_hcgemm_batched_v100PCIe} shows the respective relative speedups}. 
Recall that the cuBLAS library uses a planar 
layout where the real and the imaginary parts of the matrices are stored in separate memory spaces. However, the reported 
results do not include the timing for separating and merging these components. We only compare the execution time of the 
computational kernels with no overheads. As per the theoretical analysis in Section~\ref{sec:roofline}, there is an advantage to using interleaved layouts over planar layouts. The increased operational intensity in the former gives a performance advantage 
for the MAGMA kernel on a wider range of sizes. In fact, the speedup numbers reported in Figure~\ref{fig:speedup_hcgemm_batched_v100PCIe} 
are much more significant than those reported in Figure~\ref{fig:speedup_hgemm_batched_v100PCIe}. The reported speedups 
for the MAGMA batched HCGEMM kernel are in the range between $1.7\times$ and $7\times$. 
\begin{figure}[!htb]
\includegraphics[width=0.8\linewidth]{./results/final_perf/V100-PCIe/speedup_hcgemm_batched_v100PCIe.pdf}
\caption{Performance speedup of the batched HCGEMM kernel (interleaved layout) against cuBLAS (planar layout). 
Results are for sizes up to $256$, with \tt{batchCount} = $1000$, on a Tesla V100-PCIe GPU.}
\label{fig:speedup_hcgemm_batched_v100PCIe}
\end{figure}
