\section{The FP16 Tensor Cores in GPUs}
\label{sec:tc}
The CUDA Toolkit is one of the first programming models to provide half-precision (i.e., FP16) arithmetic. 
Early support was added in late 2015 for selected embedded GPU models that are based on the Maxwell architecture. 
The FP16 arithmetic has become mainstream in CUDA-enabled GPUs since the Pascal architecture. 
In general, half precision has a dynamic range that is significantly smaller than single or double precisions. 
Incorporating such a reduced precision was mainly motivated by the disruptive emergence of machine learning applications.  

The Volta and Turing architectures introduce hardware acceleration for matrix multiplication in FP16. 
The hardware acceleration units are called Tensor Cores. They can deliver a theoretical peak performance that is 
up to $8\times$ faster than the peak FP32 performance. As an example, each Volta V100 GPU has $640$ Tensor Cores, 
evenly distributed across $80$ multiprocessors. Each Tensor Core possesses a mixed-precision $4\times 4\times 4$ matrix 
processing array which performs the operation $D = A\times B + C$, where $A$, $B$, $C$ and $D$ are $4\times 4$ matrices. 
The inputs $A$ and $B$ must be represented in FP16 format, while $C$ and $D$ can be represented in FP16 or in 
FP32 formats. It is also possible that $C$ and $D$ point to the same matrix. 

The vendor library (cuBLAS) provides various optimized routines, mostly GEMMs, that can take advantage of the 
Tensor Core acceleration by setting the proper flag. As an example, the routine {\texttt{cublasHgemmBatched}} implements 
the batched GEMM operation for real FP16 arithmetic. All matrices are assumed to have the same dimensions. Considering 
complex FP16 computations, there is no native support for half-complex precisions yet in the library. In fact, the only way 
to use cuBLAS is to use a ``planar layout'' for the matrices, where the real and the imaginary parts of the matrices are separated. 
The planar layout enables an easy solution using existing cuBLAS routines, but it lacks an important performance advantage, which 
will be discussed later in the paper. The same concept of split-complex computation applies to the 
cuBLASLt library\footnote{\url{https://docs.nvidia.com/cuda/cublas/index.html\#using-the-cublasLt-api}}, 
as well as the open-source CUTLASS library.\footnote{\url{https://github.com/NVIDIA/cutlass}}.

Taking advantage of the Tensor Cores in a custom kernel is possible through the use of low-level APIs that are provided by the 
programming model as well. As shown in Figure~\ref{fig:tc}, Tensor Cores deal with input and output data through 
opaque data structures called \emph{fragments}. Each fragment is used to store one matrix. Fragments can be loaded from 
shared memory or from global memory using the {\texttt{load\_matrix\_sync()}} API. A similar 
API is available for storing the contents of an output fragment into the shared/global memory of the GPU. 
The {\texttt{mma\_sync()}} API is used to perform the multiplication. The user is responsible for declaring the 
fragments as required, and calling the APIs in the correct sequence. 
%=========================================================================================
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{./figures/tc.pdf}
\caption{Programmability of the Tensor Core units}
\label{fig:tc}
\end{figure}

The programming model imposes some restrictions to the programming of the Tensor Cores. First, the GEMM dimensions  
($M$, $N$, $K$), which also control the size of the fragments, are limited to three discrete combinations, namely 
($16$, $16$, $16$), ($32$, $8$, $16$), and ($8$, $32$, $16$). Second, the operations of load, store, and multiply 
must be performed by one full warp (32 threads). Finally, the load/store APIs require that the leading dimension 
of the corresponding matrix be multiple of $16$-bytes. As an example, a standard GEMM operation of size 
($16$, $16$, $16$) requires three {\texttt{load\_matrix\_sync()}} calls (for $A$, $B$, and $C$), one {\texttt{mma\_sync()}} call, 
and then a final {\texttt{store\_matrix\_sync()}} call to write the result. The latest CUDA version to date (10.1) provides direct 
access to the Tensor Cores through an instruction called {\texttt{mma.sync}}. The instruction allows one warp to perform four independent 
GEMM operations of size ($8$, $8$, $4$). However, using the explicit instruction may lead to long-term compatibility issues as new architectures are 
released. This is why we decide not to consider using parallel thread execution (PTX) instructions, and focus only on the device-level CUDA APIs.  
