\section{Optimization for Extremely Small Matrices}
\label{sec:small_opt}
The Tensor Core APIs have discrete configurations (\tt{TC\_M}, \tt{TC\_N}, \tt{TC\_K}). For batched 
GEMM problems with sizes smaller than these configurations, the TC utilization is below $100$\%, and 
depending on the problem size, the use of the TCs might be questionable. This section focuses on 
performance optimization for small sizes that cannot fully occupy the TCs when using the vendor-provided APIs. 
Without loss of generality, we are looking into small square matrices whose dimensions are $\leq 16$. This range of sizes has been 
subject to many research efforts recently, due to its popularity in many applications~\cite{abdelfattah2016high}\cite{kim2017designing}. 

\subsection{Multiple GEMMs in One Tensor Core Multiplication}
Recall that the permissible sizes for the TC multiplications are ($16$, $16$, $16$), ($32$, $8$, $16$), and ($8$, $32$, $16$). 
A single TC multiplication can therefore perform $8192$ FLOPs using any of these combinations. Considering 
a square multiplication of size $4$, the operation count is $2\times 4^3 = 128$ FLOPs, which is less than $1.5$\% of a full TC multiplication. 
A previous proposition by the authors~\cite{abdelfattah2019fast} showed how to improve the utilization by performing multiple GEMM operations in a single 
call to the TC APIs. An example is shown in Figure~\ref{fig:tc_4_4_4}, which improves the utilization for 
a ($4$, $4$, $4$) GEMM from $1.5$\% to about $6$\%. While this is a $4\times$ improvement, the TC utilization is still very low. 
Figure~\ref{fig:utilization} shows the maximum achievable utilization for the TCs when performing square multiplications using 
the ($16$, $16$, $16$) configuration. The utilization in this figure is computed as $\left \lfloor \frac{16}{N} \right \rfloor \times \frac{2 \times N^3}{8192}$. 
The figure also shows the peak half-precision performance (as a percentage) without using the TC multiplications. Theoretically, a sufficiently optimized 
kernel that does not use the TC APIs can outperform the TC kernels for small sizes, $\leq 10$. 
\begin{figure}[!htb]
\centering
\includegraphics[width=0.45\textwidth]{./figures/tc_4_4_4.pdf}
\caption{Improving Tensor Core utilization by assigning multiple GEMMs at a time. Example for square matrices of size $4$, 
with all Tensor Core sizes set to $16$.}
\label{fig:tc_4_4_4}
\end{figure}
%-----------------------------------------------------------------------------------------
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\textwidth]{./results/small/tc_utilization.pdf}
\caption{The percentage of available Tensor Core compute power for square multiplications of small matrices.}
\label{fig:utilization}
\end{figure}

The low utilization percentages shown in Figure~\ref{fig:utilization} raise questions about using TCs for tiny matrices, and whether 
conventional methods (i.e., without Tensor Cores) can perform the multiplications more efficiently. In this regard, we refer to a kernel 
developed for very small matrices specifically~\cite{masliah2016high}. The kernel addresses very small square multiplications of sizes up to $32$. The main design 
idea is to use an $N\times N$ thread configuration for each GEMM, such that each thread is responsible for a single element in the output matrix. 
The code is fully unrolled for every size using C++ templates. In this paper, we use a similar kernel design that supports both half and half-complex 
arithmetic. We call this kernel \emph{magma-small}. 
%-----------------------------------------------------------------------------------------
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\textwidth]{./results/final_perf/V100-PCIe-small/hgemm_batched_smallsq_v100PCIe.pdf}
\caption{Performance of the batched HGEMM kernels. Results are for small square sizes up to $16$, with \tt{batchCount} = $10$k, on a Tesla V100-PCIe GPU.}
\label{fig:hgemm_smallsq}
\end{figure}

We tested the performance of the \emph{magma-small} kernel against the general MAGMA kernel as well as against cuBLAS, the results of which
are summarized in Figures~\ref{fig:hgemm_smallsq} and~\ref{fig:hcgemm_smallsq} for the batched HGEMM and HCGEMM kernels, respectively.  
The generic MAGMA kernel is the best performing kernel for sizes $\approx 10$ and up. For sizes smaller than $10$, the \emph{magma-small} kernel 
is the best solution, though no Tensor Cores are used. This is an interesting observation that nicely aligns with 
Figure~\ref{fig:utilization}. Suboptimal TC utilization does not give access to the full $125$ teraFLOP/s of compute power. Depending on the utilization 
(which is impacted by the problem size), the available compute power through the TCs may be lower than the available compute power 
without them. The threshold of $25$\% shown in Figure~\ref{fig:utilization} is the ratio between the full FP16 compute power in both situations. 
A utilization below this threshold may give the advantage to a kernel that does not use the TCs. This is realized in 
Figures~\ref{fig:hgemm_smallsq} and~\ref{fig:hcgemm_smallsq} , where the magma-small kernel outperforms any kernel that uses the TCs as long as the 
problem size is below $\approx 10\times 10$. For sizes larger than that, the use of TCs begins to pay off. For the batched HGEMM kernel, the observed 
speedups against cuBLAS are between $6.6\times$ and $8.4\times$. For the batched HCGEMM, the use of the interleaved layout adds an extra advantage for the MAGMA 
kernels, which score speedups up to $21.8\times$.
%-----------------------------------------------------------------------------------------
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\textwidth]{./results/final_perf/V100-PCIe-small/hcgemm_batched_smallsq_v100PCIe.pdf}
\caption{Performance of the batched HCGEMM kernels. Results are for small square sizes up to $16$, with \tt{batchCount} = $10$k, on a Tesla V100-PCIe GPU.}
\label{fig:hcgemm_smallsq}
\end{figure}
