maintex=hgemm_si

all:
	pdflatex response.tex
	pdflatex ${maintex}
	bibtex   ${maintex}
	pdflatex ${maintex}
	pdflatex ${maintex}

rev:
	pdflatex response.tex

clean:
	$(RM) *~ *.bak *%
	$(RM) *.log *.aux *.dvi *.blg *.toc *.bbl *.lof *.lot \
		*.idx *.ilg *.int *.spl *.out */*.aux 
