\section{Half-Complex Batched GEMM: Planar vs. Interleaved Layouts}
\label{sec:roofline}
In order to perform half-complex computations using the cuBLAS library, a user must use ``split-complex'' computation. The real and the imaginary parts of $A$, $B$, and $C$ must be stored separately in a planar layout. Considering dense linear algebra algorithms, planar layouts do not help achieve good performance, especially for relatively small sizes.
\bld{First}, all the existing linear algebra numerical libraries assume an \emph{interleaved layout}, meaning that the real and the imaginary parts of each element are contiguous in memory. It is not practical to rewrite entire algorithms using split-complex computations to make use of the TCs. 
\bld{Second}, while it is relatively easy to develop the GEMM kernel in planar layouts using the existing GEMM kernels, other linear algebra components might not be as straightforward. Examples are triangular solve and the pivoting stage in the LU factorization. For such operations, it is not possible to use the existing triangular solve or pivoting kernels, which leads to new developments at the BLAS level. Therefore, it is more convenient to use the standard interleaved layout. 
\bld{Third}, complex compute-bound kernels normally reach the peak performance of the underlying hardware earlier than their counterparts that use real arithmetic. This is because complex kernels have more \emph{operational intensity} than real arithmetic kernels. \myc{The operational intensity of an operation is the ratio between the number of flops and the number of bytes transferred for that operation}. As an example, the real FP16 scalar operation ($c = c + a\times b$) costs 2 FLOPs (1 addition and 1 multiplication), which results in $\frac{2}{8} = 0.25$ FLOP/byte ratio. Using complex FP16 arithmetic, the same operation would cost 8 FLOPs. The product $a\times b$ costs 6 FLOPs (4 multiplications and 2 additions), and the update of $c$ costs 2 more additions. This results in $\frac{8}{16} = 0.5$ FLOP/byte ratio (double the operational intensity of the real scalar operation). 

We theoretically investigate the half-complex GEMM operation based on the Roofline model~\cite{williams2009roofline}. Our focus is on batched square multiplications ($M = N = K$), as well as batched rank-k updates ($M = N$, $K$ is a relatively small constant). The former use case demonstrates a test for the peak performance of the kernel, while the latter is an important use case in batched linear algebra, especially in the batched LU factorization. 

%-----------------------------------------------------------------------------------------
\subsection{\myc{Roofline for HCGEMM (standard interleaved layout)}}
\label{subsec:interleaved_roofline}
\myc{For simplicity of our analysis, we can safely ignore the scaling by $\alpha$ and $\beta$}. According to the LAPACK Working Note \#41 \footnote{\url{http://www.netlib.org/lapack/lawns/lawn41.ps}}, a standard GEMM operation \myc{($C_{M\times N} = C_{M\times N} + A_{M\times K} B_{K\times N}$)} involves ($M\times N\times K$) additions, and ($M\times N\times K$) multiplications. 
\myc{This can be explained as follows. The output $C$ matrix has $M\times N$ elements. Each element $c_{ij}$ is computed as $c_{ij}=c_{ij} + \sum_{k=1}^{K}a_{ik}b_{kj}$, which accounts for $K$ multiplications and $K$ additions. Therefore, the HCGEMM-interleaved operation requires $MNK$ additions and $MNK$ multiplications. In order to estimate the number of flops, we recall that one multiplication of complex numbers requires 6 FLOPs and one addition requires 2 flops. The total number of flops is, therefore, $8MNK$}. 
 
\myc{In order to derive a roofline (i.e. a performance upper-bound), we derive the ideal (minimum) amount of memory traffic for the operation.} The ideal amount of data required for HCGEMM is 
\begin{enumerate}
    \item \myc{One read and one write for $C$, which equals $2\times M\times N$} 
    \item \myc{One read for $A$ ($M\times K$)} 
    \item \myc{One read for $B$ ($K\times N$)}
\end{enumerate}

\myc{Considering half-complex precision, each element is stored in 4-bytes. Therefore, the ideal amount of bytes transferred is $4\times[2\times M\times N + K(M + N)]$. Now we can write:}
\begin{equation}
\tt{Operational intensity of HCGEMM-interleaved=$\frac{2MNK}{2MN+K(M+N)}$}
\end{equation}
%The final operational intensity of the HCGEMM-interleaved becomes $\frac{2MNK}{2MN+K(M+N)}$. 

%-----------------------------------------------------------------------------------------
\subsection{\myc{Roofline for HCGEMM (planar layout)}}
\myc{We now estimate the roofline for the HCGEMM-planar operation similar to Section~\ref{subsec:interleaved_roofline}.}
For a split-complex computation, the operation can be done via four calls to the standard HGEMM operation. \myc{Assuming that we have two separate output matrices $C_r$ (real) and $Ci$ (imaginary), the four HGEMM calls are:} 
\begin{enumerate}
    \item \myc{First call: $C_r = C_r + A_r\times B_r$} 
    \item \myc{Second call: $C_r = C_r - A_i\times B_i$} 
    \item \myc{Third call: $C_i = C_i + A_r\times B_i$} 
    \item \myc{Fourth call: $C_i = C_i + A_i\times B_r$} 
\end{enumerate}

\myc{Each call involves $MNK$ additions and $MNK$ multiplications. Since these calls perform real half-precision arithmetic, each addition/multiplication accounts for one FLOPs. Therefore, one call performs $2MNK$ FLOPs, leading to a total of $8MNK$ FLOPs. From an operation count point of view, these four calls have the same complexity as HCGEMM-interleaved. However, the collective memory traffic for these calls is different. Each call to HGEMM involves $2MN$ traffic for $C$, an $MK$ traffic for $A$, and an $KN$ traffic for $B$. The memory traffic for one HGEMM call is $2$-bytes$\times[2\times M\times N + K(M + N)]$, leading to an aggregate traffic of $8\times[2\times M\times N + K(M + N)]$. Similarly, we can write}
\begin{equation}
\tt{Operational intensity of HCGEMM-planar=$\frac{2MNK}{2MN+K(M+N)}$}
\end{equation}

%For this kernel, each addition or multiplication accounts for one FLOP, which yields a total of ($2MNK$) FLOPs. The amount of data transfer for HGEMM is $2[2\times M\times N + K(M + N)]$. The final operational intensity for HGEMM is $\frac{MNK}{2MN+K(M+N)}$. Now consider the HCGEMM kernel on an interleaved layout. Each addition accounts for two FLOPs, and each multiplication accounts for six FLOPs. A similar analysis would give an operational intensity for the HCGEMM kernel that is equal to $\frac{2MNK}{2MN+K(M+N)}$. Our final conclusion is that \emph{the HCGEMM kernel on an interleaved layout has double the operational intensity of the HCGEMM kernel on a planar layout. For complex arithmetic, interleaved layouts can deliver more performance than planar layouts}. 

\myc{To summarize, HCGEMM-interleaved has double the operational intensity of HCGEMM-planar}. For a square multiplication ($M = N = K$), the operational intensity is simplified to $0.25N$ for HCGEMM-planar, and to $0.5N$ for HCGEMM-interleaved. The roofline model~\cite{williams2009roofline} estimates the performance upper bound as \tt{operational intensity $\times$ the peak memory bandwidth}. Using a GPU STREAM benchmark on the Tesla V100 GPU, we got up to 847 GB/s of peak memory bandwidth. Figure~\ref{fig:roofline_sq} shows the performance upper bounds for HCGEMM-planar and HCGEMM interleaved for square matrices. Both graphs grow linearly with the matrix size until they hit the GPU peak performance. However, due to the increased operational intensity, HCGEMM-interleaved reaches the peak earlier than HCGEMM-planar. This is very crucial for relatively small sizes. As an example, HCGEMM-planar is bandwidth-limited at size $300$, while HCGEMM-interleaved becomes compute bound. For large sizes (larger than $550$ in Figure~\ref{fig:roofline_sq}), we may not see a difference between the two kernels (if properly optimized), since HCGEMM-planar will be able to saturate the GPU anyway. 
\begin{figure}[!htb]
\centering
\includegraphics[width=0.7\textwidth]{./results/roofline/roofline_sq.pdf}
\caption{Performance upper bound for batched HCGEMM on square sizes. The analysis is based on a 847 GB/s peak bandwidth (STREAM benchmark on a Tesla V100-PCIe GPU).}
\label{fig:roofline_sq}
\end{figure}

Another useful test case for GEMM is the rank-k updates. This is a very important use case in dense linear algebra. Figure~\ref{fig:roofline_k16} shows a similar roofline analysis for updating a square matrix ($M=N$), with a rank-16 update ($K=16$). As per the roofline model, this is a use case that always remains bandwidth-limited on the V100 GPU, regardless of the size. The operational intensity of HCGEMM-planar will be given by $\frac{8N^2}{N^2 + 16N}$, while the HCGEMM-interleaved has its own at $\frac{16N^2}{N^2 + 16N}$. This means that HCGEMM-interleaved is theoretically $2\times$ faster than HCGEMM-planar, no matter how large $M$ and $N$ are. 
\begin{figure}[!htb]
\centering
\includegraphics[width=0.7\textwidth]{./results/roofline/roofline_k16.pdf}
\caption{Performance upper bound for batched HCGEMM for rank-16 updates. The analysis is based on a 847 GB/s peak bandwidth (STREAM benchmark on a Tesla V100-PCIe GPU).}
\label{fig:roofline_k16}
\end{figure}
