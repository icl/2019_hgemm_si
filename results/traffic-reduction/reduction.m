function h = reduction(TCM, TCN)
close all;
MAX = 128;
bm = TCM:TCM:MAX;
bn = TCN:TCN:MAX;

[BM, BN]=meshgrid(bm, bn);

reduction = (TCM+TCN).*BM.*BN ./ (TCM.*TCN.*(BM+BN));

%figure
%heatmap(x, y, v1./cublas, 'Colormap',jet);
%figure
%heatmap(x, y, v2./cublas, 'Colormap',jet);
%figure
%heatmap(x, y, v3./cublas, 'Colormap',jet);
%h = heatmap(x, y, best./cublas, 'Colormap',cool, 'FontSize',12,'CellLabelColor','none');
figure
h = heatmap(bm, bn, reduction, 'Colormap',cool, 'FontSize',16,'CellLabelFormat','%.1f')
xlabel('BLK_M');
ylabel('BLK_N');
axp = struct(gca);       %you will get a warning
axp.Axes.XAxisLocation = 'top';
title(sprintf('(TC_M, TC_N) = (%d, %d)',TCM, TCN));