set   autoscale                        	# scale axes automatically
unset log                              	# remove any log-scaling
unset label                            	# remove any previous labels
set xtic auto                          	# set xtics automatically
set ytic auto                          	# set ytics automatically


set term pdfcairo dashed size 4,2.5						#(will produce .pdf output )
set termoption dash
#unset colorbox


set lmargin 8
set rmargin 3
set tmargin 2
#=========================================================
# line styles
#solid
set style line 11    dashtype 1  lw 2.5 lc 1  pt 1 ps 0.4
set style line 12    dashtype 1  lw 2.5 lc 2  pt 1 ps 0.4
set style line 13    dashtype 1  lw 2.5 lc 3  pt 1 ps 0.4
set style line 14    dashtype 1  lw 2.5 lc 4  pt 1 ps 0.4
set style line 15    dashtype 1  lw 2.5 lc 5  pt 1 ps 0.4
set style line 16    dashtype 1  lw 2.5 lc 6  pt 1 ps 0.4
set style line 17    dashtype 1  lw 2.5 lc 7  pt 1 ps 0.4
set style line 18    dashtype 1  lw 2.5 lc 8  pt 1 ps 0.4
set style line 19    dashtype 1  lw 2.5 lc 9  pt 1 ps 0.4
set style line 110   dashtype 1  lw 2.5 lc 10 pt 1 ps 0.4
#dashed
set style line 21    dashtype 2  lw 2.5 lc 1  pt 1 ps 0.4
set style line 22    dashtype 2  lw 2.5 lc 2  pt 1 ps 0.4
set style line 23    dashtype 2  lw 2.5 lc 3  pt 1 ps 0.4
set style line 24    dashtype 2  lw 2.5 lc 4  pt 1 ps 0.4
set style line 25    dashtype 2  lw 2.5 lc 5  pt 1 ps 0.4
set style line 26    dashtype 2  lw 2.5 lc 6  pt 1 ps 0.4
set style line 27    dashtype 2  lw 2.5 lc 7  pt 1 ps 0.4
set style line 28    dashtype 2  lw 2.5 lc 8  pt 1 ps 0.4
set style line 29    dashtype 2  lw 2.5 lc 9  pt 1 ps 0.4
set style line 210   dashtype 2  lw 2.5 lc 10 pt 1 ps 0.4

set style line 31    dashtype 3  lw 2.5 lc 1  pt 1 ps 0.4
set style line 32    dashtype 3  lw 2.5 lc 2  pt 1 ps 0.4
set style line 33    dashtype 3  lw 2.5 lc 3  pt 1 ps 0.4
set style line 34    dashtype 3  lw 2.5 lc 4  pt 1 ps 0.4
set style line 35    dashtype 3  lw 2.5 lc 5  pt 1 ps 0.4
set style line 36    dashtype 3  lw 2.5 lc 6  pt 1 ps 0.4
set style line 37    dashtype 3  lw 2.5 lc 7  pt 1 ps 0.4
set style line 38    dashtype 3  lw 2.5 lc 8  pt 1 ps 0.4
set style line 39    dashtype 3  lw 2.5 lc 9  pt 1 ps 0.4
set style line 310   dashtype 3  lw 2.5 lc 10 pt 1 ps 0.4
#=========================================================
# bar styles
set style line 91 lc rgb "blue"
set style line 92 lc rgb "black"
set style line 93 lc rgb "green"
set style line 94 lc rgb "red"
#=========================================================

unset xtics
unset ytics

unset key
unset rmargin
unset label

#-----------------------------------------------------------------------------------------
set output "hgemm_batched_sq_v100PCIe.pdf"
#-----------------------------------------------------------------------------------------
unset xrange 
unset yrange 
set xrange [0:130]
set yrange [0.02:]
set title  "Batch HGEMM Performance, square sizes, Batch = 1000, Tesla V100-PCIe GPU, CUDA 10.1"
unset title
set logscale y
set xlabel "Matrix size" font ", 16" offset -2.5,-0.5
set ylabel "Tflop/s" font ", 16" offset 0,0
set grid lw 1.5
set xtics font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key right bottom box Left width 1
set key samplen 2.5 spacing 1.4 font ",16"
max2(x,y) = (x > y) ? x : y
max3(a, b, c)             = ( a > max2(b,c)         ? a : max2(b,c)         )
myoff(x,speedup) = (speedup >= 1) ? ( (x < 10000) ? (0.07*log(x)) : (1.0*log(x)) ) : -(0.5*log(x))
#set style textbox opaque border lc "blue"
set style textbox opaque border
plot  "./avg_hgemm_batched_sq.txt"  every 2   using 1:($2/1000)   ls 11 pt 7 smooth unique title " magma  "  with linespoints, \
      "./avg_hgemm_batched_sq.txt"  every 2   using 1:($3/1000)   ls 13 pt 3 smooth unique title " cublas "  with linespoints, \


#-----------------------------------------------------------------------------------------
set output "hgemm_batched_k16_v100PCIe.pdf"
#-----------------------------------------------------------------------------------------
unset xrange 
unset yrange 
set xrange [0:130]
set yrange [0.02:] 
set title  "Batch HGEMM Performance, rank-16 updates, Batch = 1000, Tesla V100-PCIe GPU, CUDA 10.1"
unset title
set logscale y
set xlabel "Matrix size (M = N, K = 16)" font ", 16" offset -2.5,-0.5
set ylabel "Tflop/s" font ", 16" offset 0,0
set grid lw 1.5
set xtics font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key right bottom box Left width 1
set key samplen 2.5 spacing 1.4 font ",16"
max2(x,y) = (x > y) ? x : y
max3(a, b, c)             = ( a > max2(b,c)         ? a : max2(b,c)         )
myoff(x,speedup) = (speedup >= 1) ? ( (x < 10000) ? (0.07*log(x)) : (1.0*log(x)) ) : -(0.5*log(x))
#set style textbox opaque border lc "blue"
set style textbox opaque border
plot  "./avg_hgemm_batched_k16.txt"  every 2   using 1:($2/1000)   ls 11 pt 7 smooth unique title " magma  "  with linespoints, \
      "./avg_hgemm_batched_k16.txt"  every 2   using 1:($3/1000)   ls 13 pt 3 smooth unique title " cublas "  with linespoints, \


#-----------------------------------------------------------------------------------------
set output "hcgemm_batched_sq_v100PCIe.pdf"
#-----------------------------------------------------------------------------------------
unset xrange 
unset yrange 
set xrange [0:260]
set yrange [0.02:]
set title  "Batch HGEMM Performance, square sizes, Batch = 1000, Tesla V100-PCIe GPU, CUDA 10.1"
unset title
set logscale y
set xlabel "Matrix size" font ", 16" offset -2.5,-0.5
set ylabel "Tflop/s" font ", 16" offset 0,0
set grid lw 1.5
set xtics font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key right bottom box Left width 1
set key samplen 2.5 spacing 1.4 font ",16"
max2(x,y) = (x > y) ? x : y
max3(a, b, c)             = ( a > max2(b,c)         ? a : max2(b,c)         )
myoff(x,speedup) = (speedup >= 1) ? ( (x < 10000) ? (0.07*log(x)) : (1.0*log(x)) ) : -(0.5*log(x))
#set style textbox opaque border lc "blue"
set style textbox opaque border
plot  "./avg_hcgemm_batched_sq.txt"  every 4   using 1:($2/1000)   ls 11 pt 7 smooth unique title " magma  "  with linespoints, \
      "./avg_hcgemm_batched_sq.txt"  every 4   using 1:($3/1000)   ls 13 pt 3 smooth unique title " cublas "  with linespoints, \

#-----------------------------------------------------------------------------------------
set output "hcgemm_batched_k16_v100PCIe.pdf"
#-----------------------------------------------------------------------------------------
unset xrange 
unset yrange 
set xrange [0:260]
set yrange [0.02:12]
set title  "Batch HGEMM Performance, rank-16 updates, Batch = 1000, Tesla V100-PCIe GPU, CUDA 10.1"
unset title
set logscale y
set xlabel "Matrix size (M = N, K = 16)" font ", 16" offset -2.5,-0.5
set ylabel "Tflop/s" font ", 16" offset 0,0
set grid lw 1.5
set xtics font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key right bottom box Left width 1
set key samplen 2.5 spacing 1.4 font ",16"
max2(x,y) = (x > y) ? x : y
max3(a, b, c)             = ( a > max2(b,c)         ? a : max2(b,c)         )
myoff(x,speedup) = (speedup >= 1) ? ( (x < 10000) ? (0.07*log(x)) : (1.0*log(x)) ) : -(0.5*log(x))
#set style textbox opaque border lc "blue"
set style textbox opaque border
plot  "./avg_hcgemm_batched_k16.txt"  every 4   using 1:($2/1000)   ls 11 pt 7 smooth unique title " magma  "  with linespoints, \
      "./avg_hcgemm_batched_k16.txt"  every 4   using 1:($3/1000)   ls 13 pt 3 smooth unique title " cublas "  with linespoints, \

#-----------------------------------------------------------------------------------------
set output "speedup_hgemm_batched_v100PCIe.pdf"
#-----------------------------------------------------------------------------------------
unset xrange 
unset yrange 
set xrange [0:130]
set yrange [0.02:]
set title  "Speedup for Batch HGEMM Performance, Batch = 1000, Tesla V100-PCIe GPU, CUDA 10.1"
unset title
unset logscale y
set xlabel "Problem size" font ", 16" offset -2.5,-0.5
set ylabel "Speedup" font ", 16" offset 0,0
set grid lw 1.5
set xtics font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key right top box Left width 1
set key samplen 2.5 spacing 1.4 font ",16"
max2(x,y) = (x > y) ? x : y
max3(a, b, c)             = ( a > max2(b,c)         ? a : max2(b,c)         )
myoff(x,speedup) = (speedup >= 1) ? ( (x < 10000) ? (0.07*log(x)) : (1.0*log(x)) ) : -(0.5*log(x))
#set style textbox opaque border lc "blue"
set style textbox opaque border
plot  "./avg_hgemm_batched_sq.txt"   every 1   using 1:($2/$3)   ls 11 pt 7 smooth unique title " speedup (square sizes)"  with lines, \
      "./avg_hgemm_batched_k16.txt"  every 1   using 1:($2/$3)   ls 13 pt 3 smooth unique title " speedup (rank-16 updates) "  with lines, \


#-----------------------------------------------------------------------------------------
set output "speedup_hcgemm_batched_v100PCIe.pdf"
#-----------------------------------------------------------------------------------------
unset xrange 
unset yrange 
set xrange [0:260]
set yrange [0.02:]
set title  "Speedup for Batch HCGEMM Performance, Batch = 1000, Tesla V100-PCIe GPU, CUDA 10.1"
unset title
unset logscale y
set xlabel "Problem size" font ", 16" offset -2.5,-0.5
set ylabel "Speedup" font ", 16" offset 0,0
set grid lw 1.5
set xtics font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key right top box Left width 1
set key samplen 2.5 spacing 1.4 font ",16"
max2(x,y) = (x > y) ? x : y
max3(a, b, c)             = ( a > max2(b,c)         ? a : max2(b,c)         )
myoff(x,speedup) = (speedup >= 1) ? ( (x < 10000) ? (0.07*log(x)) : (1.0*log(x)) ) : -(0.5*log(x))
#set style textbox opaque border lc "blue"
set style textbox opaque border
plot  "./avg_hcgemm_batched_sq.txt"   every 1   using 1:($2/$3)   ls 11 pt 7 smooth unique title " speedup (square sizes)"  with lines, \
      "./avg_hcgemm_batched_k16.txt"  every 1   using 1:($2/$3)   ls 13 pt 3 smooth unique title " speedup (rank-16 updates) "  with lines, \

