set   autoscale                        	# scale axes automatically
unset log                              	# remove any log-scaling
unset label                            	# remove any previous labels
set xtic auto                          	# set xtics automatically
set ytic auto                          	# set ytics automatically


set term pdfcairo dashed size 5,3.125						#(will produce .pdf output )
set termoption dash
#unset colorbox


set lmargin 9
set rmargin 3
set tmargin 2
set bmargin 5
#=========================================================
# line styles
#solid
set style line 11    dashtype 1  lw 2.5 lc 1  pt 1 ps 0.75
set style line 12    dashtype 1  lw 2.5 lc 2  pt 1 ps 0.75
set style line 13    dashtype 1  lw 2.5 lc 3  pt 1 ps 0.75
set style line 14    dashtype 1  lw 2.5 lc 4  pt 1 ps 0.75
set style line 15    dashtype 1  lw 2.5 lc 5  pt 1 ps 0.75
set style line 16    dashtype 1  lw 2.5 lc 6  pt 1 ps 0.75
set style line 17    dashtype 1  lw 2.5 lc 7  pt 1 ps 0.75
set style line 18    dashtype 1  lw 2.5 lc 8  pt 1 ps 0.75
set style line 19    dashtype 1  lw 2.5 lc 9  pt 1 ps 0.75
set style line 110   dashtype 1  lw 2.5 lc 10 pt 1 ps 0.75
#dashed
set style line 21    dashtype 2  lw 2.5 lc 1  pt 1 ps 0.75
set style line 22    dashtype 2  lw 2.5 lc 2  pt 1 ps 0.75
set style line 23    dashtype 2  lw 2.5 lc 3  pt 1 ps 0.75
set style line 24    dashtype 2  lw 2.5 lc 4  pt 1 ps 0.75
set style line 25    dashtype 2  lw 2.5 lc 5  pt 1 ps 0.75
set style line 26    dashtype 2  lw 2.5 lc 6  pt 1 ps 0.75
set style line 27    dashtype 2  lw 2.5 lc 7  pt 1 ps 0.75
set style line 28    dashtype 2  lw 2.5 lc 8  pt 1 ps 0.75
set style line 29    dashtype 2  lw 2.5 lc 9  pt 1 ps 0.75
set style line 210   dashtype 2  lw 2.5 lc 10 pt 1 ps 0.75

set style line 31    dashtype 3  lw 2.5 lc 1  pt 1 ps 0.75
set style line 32    dashtype 3  lw 2.5 lc 2  pt 1 ps 0.75
set style line 33    dashtype 3  lw 2.5 lc 3  pt 1 ps 0.75
set style line 34    dashtype 3  lw 2.5 lc 4  pt 1 ps 0.75
set style line 35    dashtype 3  lw 2.5 lc 5  pt 1 ps 0.75
set style line 36    dashtype 3  lw 2.5 lc 6  pt 1 ps 0.75
set style line 37    dashtype 3  lw 2.5 lc 7  pt 1 ps 0.75
set style line 38    dashtype 3  lw 2.5 lc 8  pt 1 ps 0.75
set style line 39    dashtype 3  lw 2.5 lc 9  pt 1 ps 0.75
set style line 310   dashtype 3  lw 2.5 lc 10 pt 1 ps 0.75
#=========================================================
# bar styles
set style line 91 lc rgb "blue"
set style line 92 lc rgb "black"
set style line 93 lc rgb "green"
set style line 94 lc rgb "red"
#=========================================================

unset xtics
unset ytics

unset key
unset rmargin
unset label


#--------------------
unset yrange
set xrange [0:17]
set yrange [0:]
set output "tc_utilization.pdf"
set title  "Utilization of tensor core compute power for square matrix multiplications"
unset title
set xlabel "Matrix size" font ", 16" offset -2.5,-1
set ylabel "Compute power utilized (\%)" font ", 16" offset 0,2
set grid lw 1.5
set xtics 1 font ", 16" offset 1,-0.5 rotate by 0 right
set ytics font ", 16" offset 0,0
set key left top box Left width 1
unset label
unset key
set style textbox opaque border
set label "Peak FP16 Performance (No Tensor Cores)" at 1,31 rotate by 0 font "Times Bold, 16" boxed
plot  "./tc_utilization.txt"  every 1 using 1:2 ls 11 pt 7 smooth unique notitle  with linespoints, \
      "./tc_utilization.txt"  every 1 using 1:3 ls 23      smooth unique notitle  with linespoints, \
      
