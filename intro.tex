\section{Introduction}
\label{sec:intro}
High-performance linear algebra libraries enable scientific applications to run efficiently 
on massively parallel architectures. Considering dense linear algebra software, high performance 
is usually achievable through algorithmic designs that express as many computational stages as possible 
in terms of compute-bound routines in general, and matrix multiplication (GEMM) in particular. The GEMM kernel 
is not only embarrassingly parallel; it also has a relatively high arithmetic intensity~\cite{williams2009roofline}, 
which is defined as the ratio between the amount of floating-point operations (FLOPs) and the number of bytes 
transferred to/from the main memory. These two properties make the GEMM kernel extremely important for numerous 
computational domains. According to the standard interface of Basic Linear Algebra Subprograms (BLAS), 
a standard GEMM operation updates a matrix $C$, where $C{_{\textsubscript M\times \textsubscript N}} = \alpha A{_{\textsubscript M\times \textsubscript K}}\times B{_{\textsubscript K\times \textsubscript N}} + \beta C{_{\textsubscript M\times \textsubscript N}}$.
Both $\alpha$ and $\beta$ are scalars. The total number of FLOPs in a GEMM operation is equal to 
$(2 M N K)$ . The amount of bytes transferred is equal to $P\times [K(M+N) + 2 MN]$, where $P$ 
is the number of bytes required to represent a floating-point number in a specific precision. We 
consider real and complex half-precision arithmetic in this paper. For real FP16 arithmetic (HGEMM), $P$ is equal to 
$2$, while for the Half-Complex GEMM (i.e., HCGEMM), $P$ is equal to $4$. The name HCGEMM does not follow the standard 
Basic Linear Algebra Subprograms (BLAS) notation, which assigns a single letter prefix to denote the precision. In fact, there is no consensus to date 
for naming linear algebra operations in half-complex precision. The term HCGEMM is used only within this paper, and may 
not be used in the final API of the released software. 
 
The GEMM kernel is also important in many scientific domains other than dense linear algebra, especially where 
the computational workload can be broken down into a large number of small matrix operations. The workload is often 
called a ``batched workload,'' and dedicated routines have been optimized for such workloads (e.g., batched GEMMs). 
It turns out that the batched GEMM kernel is almost as important as the regular non-batched GEMM, since it has been 
featured in many applications, such as sparse direct solvers~\cite{yeralan2017algorithm}, and tensor 
contractions~\cite{abdelfattah2016high}. Some hardware vendors now provide optimized batched GEMM kernels, such as 
the Intel Math Kernel Library (MKL) library\footnote{\url{https://software.intel.com/mkl}} and the NVIDIA cuBLAS 
library\footnote{\url{https://developer.nvidia.com/cublas}}. The latter provides an optimized batched HGEMM kernel as well. 
However, for the complex case, the vendor advises the use of ``split-complex'' computations, which assumes 
that the real and the imaginary parts of the matrix are separated in a ``planar layout.'' 

The artificial intelligence (AI) and machine learning revolution has created a huge demand for high-performance half-precision 
arithmetic ($16$-bit floating-point format), since most AI applications do not necessarily require the 
accuracy of single or double precisions~\cite{gupta2015deep}. In terms of memory bandwidth and footprint, half precision 
theoretically offers a natural $2\times$ improvement over single precision, and $4\times$ over double precision. With respect to 
floating-point operations, the improvement factors over single and double precision depends on the architectural properties of 
the hardware. In this paper, we target NVIDIA's Volta GPUs, which offer native FP16 arithmetic. The theoretical peak performance for 
``general'' FP16 computation is about $31.25$ tera floating-point operations per second (teraFLOP/s). This is double the peak single precision performance of 
$15.6$ teraFLOP/s, and four times the peak double precision performance of $7.8$ teraFLOP/s. However, Volta GPUs offer even more performance for 
special operations in FP16 arithmetic. These operations are mostly variations of matrix multiplication, and are hardware-accelerated using 
special units called Tensor Cores (TCs). For such special operations, the performance of a single GPU can reach up to $125$ teraFLOP/s. 
Although the Tensor Cores units first appeared in Volta GPUs, NVIDIA's first GPUs to ever support half precision were the Pascal GPUs. 
Half precision on NVIDIA GPUs implements the ``binary16'' format which is defined by the IEEE-754 standard~\cite{ieee754}. As shown in 
Figure~\ref{fig:fp16}, half precision uses one bit for the sign, five bits for the exponent, and ten bits for the fraction. However, 
the format assumes an implicit leading bit that is set to one unless the exponent is all zeros. This gives the FP16 format a total 
of eleven bits for the fraction, which accounts for an accuracy of about three decimal digits.  
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\linewidth]{./figures/fp16_layout.pdf}
\caption{Half precision format according to the IEEE-754 standard}
\label{fig:fp16}
\end{figure}

This paper investigates the use of the Tensor Cores to provide a general-purpose batched matrix multiplication in FP16 arithmetic. 
The paper extends the previous effort by the authors~\cite{abdelfattah2019fast} by considering half-complex computation on the GPU, 
which is not natively supported to the best of our knowledge. While half-complex precision may be of limited use in the machine learning domain, 
it is of significant value in linear algebra, especially with respect to mixed-precision solvers. The paper addresses some challenges in using 
the Tensor Cores programmatically in a GPU kernel, such as discrete sizes and restricted thread configurations. The kernel design is expressed 
in terms of ``building blocks,'' which are developed as device routines that perform specific tasks in the kernel. Some of these building 
blocks are shared across half and half-complex precisions, but some other functions had to be explicitly developed for each precision. An 
important goal was to provide highly flexible kernels that can withstand potential future changes to the Tensor Core technology. 
The developed kernels have at least eight tuning parameters that control different aspects of the kernel. An extensive tuning process has 
been applied to the developed kernels, with respect to typical use cases for batched GEMM operations. We also investigate the benefit of 
using Tensor Cores for extremely small problems, where full utilization of the Tensor Core units is not possible. 
While the vendor routine is very optimized for relatively large sizes, we observe that the batched HGEMM kernel outperforms cuBLAS for sizes 
 $\leq 128$ with speedups that range between $1.5\times$ and $2.5\times$. We also show that the batched HCGEMM kernel outperforms the 
vendor solution (which uses split complex computation) by improvement factors between $1.7\times$ and $7\times$. This work is part of the 
open-source MAGMA library~\cite{gullo2009numerical}~\footnote{\url{https://icl.cs.utk.edu/magma/}}. 
  
 