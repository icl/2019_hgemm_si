\section{General Design Outlines}
\label{sec:general_design}
This section describes some general concepts about the design of the batched HGEMM and the batched HCGEMM kernels. 
The dimensions ($M$, $N$, $K$) of the GEMM operation are assumed to be unified across the batch. According 
to the CUDA programming model, a GPU kernel is, in general, a three-dimensional grid of three-dimensional 
thread blocks (TBs). The number of GEMM operations in the batch is referred to as \tt{batchCount}. For real 
half-precision computation, the CUDA data type \tt{\_\_half} is used. For half-complex computations, we use the 
\tt{\_\_half2} vector type. The low $16$-bits represent the real part, while the high $16$-bits represent the 
imaginary part. 

\begin{figure}[!htb]
\centering
\includegraphics[width=0.6\textwidth]{./figures/gemm_grid.pdf}
\caption{Organization of the batched GEMM grid}
\label{fig:grid}
\end{figure}

%-----------------------------------------------------------------------------------------
\subsection{Grid Design}
\label{subsec:grid}
The MAGMA library uses a common grid design for all of its batched kernels~\cite{abdelfattah2016performance, abdelfattah2017novel}. 
The output matrices are subdivided into smaller blocks that can fit into a fast memory level (i.e., registers or shared memory). 
Such blocks can be square or rectangular, with their sizes denoted as (\tt{BLK\_M}$\times$\tt{BLK\_N}). The first two dimensions of the grid 
are used to denote a two-dimensional $\left \lceil \frac{M}{\tt{BLK\_M}} \right \rceil \times \left \lceil \frac{N}{\tt{BLK\_N}} \right \rceil$ 
``subgrid'' for each output matrix in the batch. The third grid dimension is used for batching across the problems, which yields a 
three-dimensional grid configuration of   
($\left \lceil \frac{M}{\tt{BLK\_M}} \right \rceil$, $\left \lceil \frac{N}{\tt{BLK\_N}} \right \rceil$, \tt{batchCount}). 
Each subgrid has a unique \tt{batchid} (the $z-$dimension of the grid) and takes care of a single GEMM operation. 
Similarly, the input matrices $A$ and $B$ are subdivided into smaller blocks of sizes (\tt{BLK\_M}$\times$\tt{BLK\_K}) 
and (\tt{BLK\_K}$\times$\tt{BLK\_N}), respectively. Within every subgrid, each TB is responsible for computing a block of 
the output matrix by reading a block row of $A$ and a block column of $B$. The block rows/columns are read in steps of \tt{BLK\_K}. 
At each iteration of the main loop, a TB multiplies a block of $A$ (\tt{BLK\_M}$\times$\tt{BLK\_K}) with a block of $B$ 
(\tt{BLK\_K}$\times$\tt{BLK\_N}). Since we are using the Tensor Core units, the accumulations of the partial results takes place 
in the fragments rather than regular register buffers or shared memory. Figure~\ref{fig:grid} illustrates the TB organization of the kernel.  

The following sections describe the main design aspects of the kernel, which leverages some design concepts from existing 
kernels~\cite{abdelfattah2016performance} while modifying or generalizing them to take advantage of the Tensor Cores. \todo{Check my edit here. I assumed you were talking about the kernel as subject rather than the design aspects. }

%=========================================================================================
\section{Detailed Thread Block Design}
\label{sec:tb_design}

\subsection{Abstracting Tensor Cores}
\label{sec:abstracting}
As mentioned before, the use of the Tensor Cores programmatically must follow some constraints 
that are required by the device-level APIs. Our goal in this paper is to design GPU kernels 
with an abstraction layer over the Tensor Cores. The abstraction layer takes care of the 
device-API constraints and provides a general-purpose use of the cores. 
The three main constraints for using Tensor Cores are:
\begin{enumerate}
\item Tensor Cores can be used only with three discrete combinations of blocking sizes
\item Device-level APIs must be called by a single warp
\item Loading/storing fragments must be from/to memory spaces with specific leading dimensions 
(multiples of $16$ bytes). 
\end{enumerate}

The abstraction layer proposed in this paper addresses each one of these constraints. 
Eventually, the GPU kernels will be able to use arbitrarily large blocking sizes, 
using any number of warps, with no constraints on the leading dimension of the 
matrices. 

%\subsection{Matrices with Arbitrary Leading Dimensions}
The developed solution must support arbitrary leading dimensions for $A$, $B$, and $C$ in the global 
memory of the GPU. A straightforward solution is to read the matrices into shared-memory buffers, rather 
than reading them directly into the Tensor Core fragments. The shared-memory buffers are allocated with 
leading dimensions that abide by the $16$-byte rule.  

%=========================================================================================
\subsection{Double-Sided Recursive Blocking (DSRB)}
\label{sec:dsrb}
This technique allows GPU kernels to use arbitrarily large blocking sizes (\tt{BLK\_M}, 
\tt{BLK\_N}, \tt{BLK\_K}) that are not necessarily restricted to the Tensor Core sizes 
(\tt{TC\_M}, \tt{TC\_N}, \tt{TC\_K}). Recursive blocking is a well-known technique that has 
been used for years in previous GEMM designs~\cite{nath2010improved}. It transfers each block 
of $A$, $B$, and $C$ to/from the global memory using a two-dimensional thread configuration 
\tt{DIM\_X}$\times$\tt{DIM\_Y}. These blocks are subdivided into smaller \tt{DIM\_X}$\times$\tt{DIM\_Y} tiles 
that can be stored in shared memory or in the register file. The same \tt{DIM\_X}$\times$\tt{DIM\_Y} subdivision 
is used for computing the partial products of these blocks. We call this technique \emph{single-sided recursive 
blocking (SSRB)}. Such a technique is not applicable to Tensor Cores, which have discrete blocking sizes for 
computation. This is why we propose a generalization over SSRB, which we call 
\emph{double-sided recursive blocking (DSRB)}. As shown in Figure~\ref{fig:blocking}, it simply decouples the way blocks 
are read/written from the way they are passed to/from the Tensor Core units. During the read/write of a block from/to the global 
memory, a matrix block is always subdivided into \tt{DIM\_X}$\times$\tt{DIM\_Y} tiles. During the computation of a 
partial product, however, the loaded blocks are subdivided using the TC sizes (\tt{TC\_M}, \tt{TC\_N}, \tt{TC\_K}). 
Thread regrouping is also used to select the best configuration at each stage of the kernel. As an example, assume a 
single warp configuration in a kernel where all blocking sizes are equal to $16$. When loading a $16\times 16$ block of data, 
it is much better to use a $16\times 2$ or $8\times 4$ configuration rather than the default $32\times 1$ one. This is why 
we allow a single warp to reorganize itself into a \tt{DIM\_X}$\times$\tt{DIM\_Y} configuration when reading and writing 
blocks of data. During the computation, the regular $32\times 1$ configuration is used. 
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\textwidth]{./figures/double_sided_blocking.pdf}
\caption{An example of the double-sided recursive blocking (DSRB) applied to a block of $C$. The blocking dimensions 
for reads and writes are decoupled from those used for computations.}
\label{fig:blocking}
\end{figure}

\subsubsection{How Does DSRB Improve Memory Traffic?}
The DSRB technique generalizes \tt{BLK\_M}, \tt{BLK\_N}, and \tt{BLK\_K} so that they are not bound to the TC sizes. 
Generic block sizes help improve the memory traffic required by the GEMM kernel. To illustrate this point further, we simplify 
our analysis by assuming that \tt{BLK\_M} and \tt{BLK\_N} fully divide $M$ and $N$, respectively. According to our grid configuration 
in Section~\ref{subsec:grid}, the proposed kernels would require $\frac{M\times N}{\tt{BLK\_M}\times \tt{BLK\_N}}$ TBs. Theoretically, an ideal 
implementation would read $A$, $B$, and $C$ from the global memory exactly once, and write $C$ once. 
The proposed kernels perform an ideal memory traffic for $C$, since each TB takes care of one block of $C$, and so it reads and writes 
such a block exactly once. However, since the parallel work is distributed across many independent TBs, there have to be redundant memory 
loads for $A$ and $B$. The redundant loads are significantly affected by \tt{BLK\_M} and \tt{BLK\_N}. The total memory loads for $A$ and $B$ per 
TB are given by ($K\times (\tt{BLK\_M} + \tt{BLK\_N})$), since it has to read an entire block row of $A$ and a block column of $B$. The total memory 
traffic (for $A$ and $B$) for the whole kernel is given by $\frac{M N K (\tt{BLK\_M} + \tt{BLK\_N})}{\tt{BLK\_M}\times \tt{BLK\_N}}$. We can now 
calculate the improvement (reduction) in memory traffic by comparing the previous formula against the case when 
(\tt{BLK\_M}, \tt{BLK\_N}, \tt{BLK\_K}) = (\tt{TC\_M}, \tt{TC\_N}, \tt{TC\_K}), which eventually yields:
\begin{center}
Memory traffic improvement = $\frac{\tt{BLK\_M}\times \tt{BLK\_N} \times (\tt{TC\_M} + \tt{TC\_N})}{\tt{TC\_M}\times \tt{TC\_N} \times (\tt{BLK\_M} + \tt{BLK\_N})}$
\end{center}

Figure~\ref{fig:traffic} shows the relative reduction in memory traffic for blocking sizes up to $128$. As an example, 
using (\tt{BLK\_M}, \tt{BLK\_N}) = ($64$, $64$) can theoretically reduce the memory traffic by a factor of $4\times$ when 
(\tt{TC\_M}, \tt{TC\_N}) = ($16$, $16$), and by a factor of $5\times$ when (\tt{TC\_M}, \tt{TC\_N}) = ($32$, $8$). 
Such a reduction usually leads to a better performance, since most of the memory requests are fulfilled from the main 
memory due to the relatively small caches in GPUs (compared to CPUs). However, this theoretical analysis assumes infinite resources 
available for each TB. Using too many resources per TB could actually worsen other aspects of the kernel, such as the occupancy and the register pressure. 
Therefore, a tuning process is usually required to search for the best blocking sizes on a specific GPU.
\begin{figure}
\centering
\includegraphics[width=\linewidth]{./results/traffic-reduction/tc_v2.pdf}
\caption{The relative reduction in memory traffic for $A$ and $B$ using (\tt{BLK\_M}, \tt{BLK\_N}) $\geq$ (\tt{TC\_M}, \tt{TC\_N}). 
Results are shown for (\tt{TC\_M}, \tt{TC\_N}) = ($16$, $16$) and ($32$, $8$).  
Note that \tt{BLK\_x} must always be multiple of \tt{TC\_x}.}
\label{fig:traffic}
\end{figure}

%=========================================================================================
\subsection{Two-Stage Loading of Input Data}
\label{sec:loading_data}
Instead of loading the blocks of $A$ and $B$ directly into the shared-memory buffers, we use a two-stage process where 
the data is first read in register buffers, then offloaded to the shared memory. The use of double buffers would allow us 
incorporate a prefetching mechanism into the register file while the data are being processed through the buffers. 
Two device-level functions have been developed for this purpose. The first one reads a block of data from the global memory 
into the register file. 
\lstinputlisting[language=C++]{./codes/read_global2reg.cpp}

The function is templated for type, block sizes (\tt{BLK\_R}$\times$\tt{BLK\_C}), as well as the thread configuration 
(\tt{DIM\_X}$\times$\tt{DIM\_Y}). These parameters are known at compile time, which enables fully unrolled loops, and 
avoids register spilling into the local memory. The \tt{fetch<T>()} function returns either the required element, or zero if the 
passed address is out-of-bound. This function is one of the shared building blocks across half and half-complex 
kernels. 

The second device function offloads the content of a register buffer into a shared-memory space. A similar structure of two nested loops 
is used. 
\lstinputlisting[language=C++]{./codes/store_reg2smem.cpp}

Unlike the previous device function, the \tt{store\_reg2smem} function cannot be used as it is for half-complex buffers. 
Since the TCs support only real arithmetic, we need to split the contents of the register file into two separate 
memory spaces; one for the real part, and the other for the imaginary part. The function below shows the corresponding 
\tt{stroe\_reg2smem\_complex} function, which uses low-level intrinsics for splitting the values. 
\lstinputlisting[language=C++]{./codes/store_reg2smem_complex.cpp}

%\subsubsection{Pointer Redirecting versus Padding}
%The design of blocking techniques in GPU kernels always encounters an issue for arbitrary matrix sizes that 
%are not fully divisible by the blocking sizes. While padding is the conventional technique, some of the kernels 
%in the MAGMA library use a different technique called \emph{pointer redirecting}, which tries to avoid branching 
%by using pointer arithmetic to redirect out-of-bound memory accesses to be within the given matrix. This technique 
%has been used in the GEMM kernel~\cite{nath2010improved}, the batch GEMM kernel~\cite{abdelfattah2016performance}, 
%and other kernels as well~\cite{nath2011optimizing}. 
%
%However, pointer redirecting techniques are always associated with careful programming of the compute part of the kernel, 
%since the data blocks in shared memory (or registers) are eventually padded with non-zero values. As an example, the compute 
%part of the GEMM operation updates the block of $C$ using a series of rank-1 updates resulting from outer products 
%between a column of $A$ and a row of $B$. This guarantees that partial blocks are computed correctly. In this paper, we 
%found that pointer redirecting is not applicable, since we do not have information about how the tensor cores actually 
%perform the matrix products. This is why our design uses the traditional padding with zeros for partial blocks of data. 

%=========================================================================================
\subsection{Multi-Warp Configuration} 
\label{sec:multi-warps}
All TC device functions must be invoked using one warp. However, this does not necessarily mean that the TB configuration 
should be restricted to a single warp. In fact, it is sometimes beneficial to use multiple warps per TB, especially when the 
amount of work per TB is relatively large, or when a warp is stalled. We generalize our thread configuration to support 
any number of warps. As mentioned before, threads reorganize themselves into a \tt{DIM\_X}$\times$\tt{DIM\_Y} configuration during 
memory operations. During computation, however, we must reorganize the threads in a $32\times \tt{N\_WARPS}$ configuration in order to 
use the TC. Note that the parameter space for \tt{DIM\_X} and \tt{DIM\_Y} is now much bigger, which serves the design flexibility. 
As an example, four warps can be used in many configurations, such as $8\times 16$, $16\times 8$, $32\times 4$, $64\times 2$, $\cdots$ etc.
When a partial product is being computed, the block accumulator of $C$ is subdivided into many sub-blocks of size \tt{TC\_M}$\times$\tt{TC\_N}. 
Warps loop over these sub-blocks in a round-robin manner. For each sub-block, the respective warp loops over the corresponding sub-block row of 
$A$ and the sub-block column of $B$, sends them in chunks to the Tensor Cores, and keeps accumulating the results in its respective fragment. 
Figure~\ref{fig:colwarps} shows the workload distribution for two different configuration on a block accumulator that has $15$ sub-blocks. 
\begin{figure}[!htb]
\centering
\includegraphics[width=0.7\textwidth]{./figures/collaborative_warps.pdf}
\caption{Multi-warp configuration with round-robin assignment for the output $C$ block.}
\label{fig:colwarps}
\end{figure}

The use of multiple warps enables controlling the amount of work per warp, especially for large blocks of data. 
Reading in large blocks is usually required to achieve a high memory bandwidth and to increase data reuse. 
But since TC multiplications must be performed by a single warp, large blocks of data could mean too much work for 
one warp, and it becomes better to involve more warps in computation. 
To better quantify this concept, we performed an experiment on a batched HGEMM on square $64\times 64$ matrices. 
For simplicity, we fix \tt{BLK\_M} = \tt{BLK\_N} = \tt{BLK\_K} = $64$, and \tt{TC\_M} = \tt{TC\_N} = \tt{TC\_K} = $16$. 
We also show two possible thread configurations, one with \tt{DIM\_X} = $16$, and the other with \tt{DIM\_X} = $32$. 
Figure~\ref{fig:colwarps_perf} shows that increasing the number of warps per TB leads to significant performance gains when the blocks are 
relatively large (while fixing all other parameters). However, the performance drops if too many warps are used per TB. 
One reason is the occupancy, which impacts the number of live TBs that can be scheduled by the runtime on the same multiprocessor. Another 
reason is that some warps may be idle during the compute phase. This appears in Figure~\ref{fig:colwarps_perf} when the number of warps 
is set to $32$. With blocking sizes set to $64$ and Tensor Core sizes set to $16$, we have a $4\times 4$ sub-block organization, which 
means that $16$ warps out of the $32$ are not assigned to any computational workload. 
\begin{figure}[!htb]
\centering
\includegraphics[width=0.7\textwidth]{./results/colwarps/collaborative_warps.pdf}
\caption{The impact of the number of warps on performance for relatively large blocking sizes. 
Results are shown for square $64\times 64$ matrices using a Tesla V100 GPU, with \tt{batchCount = 500}. CUDA version is 10.1. 
All blocking sizes are set to 64, with all Tensor Core sizes set to 16. \tt{DIM\_Y} = (\#warps$\times 32$)/\tt{DIM\_X}.}
\label{fig:colwarps_perf}
\end{figure}

%=========================================================================================
\subsection{Performing the Multiplication Using the Tensor Cores} 
\label{sec:product}
After loading the input data blocks in the shared-memory buffers, another device function (\tt{tc\_multiply()}) 
is invoked  to perform the TC multiplication using the round-robin style illustrated before. The function 
is heavily templated with a number of constants that are known at compile time, such as \tt{TC\_BLOCKS}, \tt{NWARPS}, and \tt{NFRAG}). 
The parameter \tt{TC\_BLOCKS} refers to the total number of multiplications a thread block performs, 
while \tt{NWARPS} and \tt{NFRAG} refer to the  number of warps and the number of accumulator fragments per warp, respectively. 
The pseudocode below distributes the \tt{TC\_BLOCKS} multiplications across warps. At each iteration of the outer loop, every warp independently 
calculates the coordinates of the $C$ sub-block it should compute. It then proceeds to the innermost loop, 
where the corresponding sub-block row of $A$ is multiplied by the corresponding sub-block column of $B$ using 
the TC APIs. The code has a cleanup section that handles the situation when \tt{NWARPS} does not 
fully divide \tt{TC\_BLOCKS}.
\lstinputlisting[language=C++]{./codes/tc_multiply.cpp}

The \tt{tc\_multiply()} function does not work for half-complex arithmetic, and a dedicated function is required to do a 
split-complex multiplication on the TB level, which we call \tt{tc\_multiply\_complex()}. The function accepts two shared-memory pointers 
(real and imaginary) for each of the $A$ and $B$ blocks. The innermost loop performs four multiplications instead of one. 
The partial results are accumulated in separate output fragments. 
\lstinputlisting[language=C++]{./codes/tc_multiply_complex.cpp}

%=========================================================================================
\subsection{Post Processing} 
\label{sec:post}
Recall that the GEMM operation is defined as 
$C{_{\textsubscript M\times \textsubscript N}} = \alpha A{_{\textsubscript M\times \textsubscript K}}\times B{_{\textsubscript K\times \textsubscript N}} + \beta C{_{\textsubscript M\times \textsubscript N}}$.
So far, all of the different computational stages serve for computing the $A\times B$ product. The scaling operations by 
$\alpha$ and $\beta$ are performed at a post-processing stage. For the batched HGEMM kernel, the post-processing stage loads 
the respective block of $C$ into a register buffer and scales it by $\beta$. In order to save memory traffic, this step takes place 
only if $\beta$ is a non-zero. The product $A\times B$ resides in shared memory. It is scaled by $\alpha$ and added to the the register buffer 
of $C$ before finally writing it to the global memory. As for the half-complex case (batched HCGEMM), recall that the \tt{tc\_multiply\_complex()} function 
has the real and imaginary parts of the product separated. The product $A\times B$ is therefore merged first in the shared memory 
before any scaling takes place. The rest of the post-processing step is similar to the batched HGEMM kernel. 

%=========================================================================================
\subsection{The Main Kernel Structure} 
\label{sec:whole_kernel}
The pseudocode below shows the main loop of the kernel. At each iteration, a pair of data blocks---from $A$ and $B$---is loaded from global memory to shared memory (in two stages). The actual dimensions 
of these blocks (\tt{am, an, bm, bn}) are computed at the beginning of the iteration so that 
the \tt{read\_global2reg()} function accounts for partial blocks by means of zero-padding if required. 
Synchronization is required to make sure all data are visible to all warps before proceeding to the multiplication 
subroutine \tt{tc\_multiply()}. The multiplication takes place simultaneously while reading a new pair for data blocks. 
Another synchronization point is required to make sure all warps are done with the currently loaded data, and 
that it is safe to overwrite the contents of the shared memory. 
\lstinputlisting[language=C++]{./codes/mainloop.cpp}

%\subsubsection{Summary of Design Contributions}
%\begin{algorithm}
%\begin{algorithmic}
%\State A = B \\
%\end{algorithmic}
%\end{algorithm}
% grid level
% tb level : choose which version

