template<...>
__device__ __inline__ void 
tc_multiply_complex(
  half* sAr, half* sAi, half* sBr, half* sBi, 
  wmma::fragment<...> fCr[NFRAG], 
  wmma::fragment<...> fCi[NFRAG] )
{
    // Declare A, B fragments
    wmma::fragment<...> fA; 
    wmma::fragment<...> fB; 

    #pragma unroll
    for(int b = 0; b < nblks; b += NWARPS){
        (i, j, fid ) <- get_next_frag_indices ( warp_id );
        #pragma unroll
        for(int k = 0; k < BLK_K; k+=TC_K){
            half* ptrAr = sAr + k * BLK_M + i;
            half* ptrAi = sAi + k * BLK_M + i;

            half* ptrBr = sBr + j * BLK_K + k;
            half* ptrBi = sBi + j * BLK_K + k;

            // sAr * sBr -> real
            wmma::load_matrix_sync(fA, ptrAr, BLK_M);
            wmma::load_matrix_sync(fB, ptrBr, BLK_K);
            wmma::mma_sync(fCr[fid], fA, fB, fCr[fid]);

            // sAr * sBi -> complex
            wmma::load_matrix_sync(fB, ptrBi, BLK_K);
            wmma::mma_sync(fCi[fid], fA, fB, fCi[fid]);

            // sAi * sBr -> complex
            wmma::load_matrix_sync(fA, ptrAi, BLK_M);
            wmma::load_matrix_sync(fB, ptrBr, BLK_K);
            wmma::mma_sync(fCi[fid], fA, fB, fCi[fid]);

            // -sAi * sBi -> real
            wmma::load_matrix_sync(fB, ptrBi, BLK_K);
            #pragma unroll
            for(int t=0; t<fA.num_elements; t++)
                fA.x[t] *= (half)(-1.0);
            wmma::mma_sync(fCr[fid], fA, fB, fCr[fid]);
        }
    }
    /* cleanup code similar to the real case */
}