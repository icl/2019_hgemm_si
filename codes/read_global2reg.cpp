template<typename T, 
const int DIM_X, const int DIM_Y, 
const int BLK_R, const int BLK_C>
static __device__ __inline__ void
read_global2reg( 
    const int blk_m, const int blk_n, 
    const T* __restrict__ A, int LDA, 
    T reg[BLK_C/DIM_Y][BLK_R/DIM_X], 
    const int tx, const int ty)
{
  int m, n;
  #pragma unroll
  for(n = 0; n < BLK_C; n+=DIM_Y) {
    #pragma unroll
      for(m = 0; m < BLK_R; m+=DIM_X) {
        reg[n/DIM_Y][m/DIM_X] = fetch<T>(/* address info */); 
    }
  }
}