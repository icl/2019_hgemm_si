template<const int DIM_X, const int DIM_Y, const int BLK_R, const int BLK_C>
static __device__ __inline__ void 
store_reg2smem_complex(
  __half2 reg[BLK_C/DIM_Y][BLK_R/DIM_X], 
  __half* sAr, __half* sAi, 
  const int tx, const int ty)
{
  int m, n;
  #pragma unroll
  for (n = 0; n < BLK_C; n+=DIM_Y) {
    #pragma unroll
    for (m = 0; m < BLK_R; m+=DIM_X) {
      sAr[(n+ty) * BLK_R + (m+tx)] = __low2half ( reg[n/DIM_Y][m/DIM_X] );
      sAi[(n+ty) * BLK_R + (m+tx)] = __high2half( reg[n/DIM_Y][m/DIM_X] );
    }
  }
}
