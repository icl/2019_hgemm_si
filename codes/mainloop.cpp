(am, an) <- compute_block_size( A, kk );
(bm, bn) <- compute_block_size( B, kk );

rA[][] <- read_global2reg<...>(am, an, A, LDA);
rB[][] <- read_global2reg<...>(bm, bn, B, LDB);

for (int kk = 0; kk < K; kk += BLK_K) {
  sA[] <- store_reg2smem( rA[][] );
  sB[] <- store_reg2smem( rB[][] );

  sync<...>();
  if(/* not last iteration*/) {
    // prefetch
    A  += BLK_K * LDA; 
    B  += BLK_K;
    (am, an) <- compute_block_size( A, kk );
    (bm, bn) <- compute_block_size( B, kk ); 
    rA[][] <- read_global2reg<...>(am, an, A, LDA);
    rB[][] <- read_global2reg<...>(bm, bn, B, LDB);
}

  tc_multiply<...>(sA[], sB[], fC[]);
  sync<...>();
}

// post processing step
rC[][] <- post_process( fC[], sC[]);

// write output
wrire_reg2global( rC[][], C[] );