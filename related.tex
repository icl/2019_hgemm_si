\section{Related Work}
\label{sec:related}
Matrix multiplication is an embarrassingly parallel operation with a relatively high operational intensity. These 
two properties enable GEMM operations to run asymptotically at $90+$\% of the GPU's peak performance. 
This good match between the properties of GEMM and GPUs being throughput-oriented processors has led to the emergence and success 
of GPU-accelerated dense linear algebra. Research efforts date back almost a decade, when GPUs started to have programmable 
shared memories (i.e., user-controlled caches). This enabled researchers to develop the first compute-bound GEMM 
on GPUs~\cite{volkov2008benchmarking}. Since then, the GEMM kernel has been subject to continuous improvements, like register 
and shared-memory blocking and prefetching~\cite{nath2010improved}. Such developments sparked many 
efforts in providing fast high-level dense linear solvers on GPUs, such as the MAGMA 
library~\cite{tomov2010towards}, ViennaCL~\cite{rupp2016viennacl}, and Chameleon~\cite{agullo2010faster}. 
Performance portability of GEMM was achieved through performance-critical tuning parameters that control different properties 
of the GEMM design~\cite{li2009note}~\cite{kurzak2012autotuning}. Following the publicly available developments 
from the research community, the GPU vendor started providing highly optimized GEMM implementations that are 
written in a low-level language~\cite{tan2011fast} in order to overcome some 
limitations imposed by the compiler and the hardware scheduler. Similarly, assembly
implementations~\cite{lai2013performance,gray2015full} are available today in the 
cuBLAS library, with the ability to achieve a performance that is very close to the GPU theoretical 
peak. Similar to the libraries mentioned above, the vendor also provides a library called 
cuSOLVER\footnote{\url{https://developer.nvidia.com/cusolver}} for high-level dense linear algebra algorithms. 

All the aforementioned efforts address the problem of one GEMM operation that is relatively large enough 
to provide sufficient parallel work for the GPU. The recent application-driven interest in batched linear solvers 
have encouraged vendors and library developers to design dedicated routines that can address a large number 
of small matrix problems. Algorithmically, a batched GEMM is still a very important 
operation, since it remains the performance key to higher-level algorithms such as the batched one-sided 
factorizations~\cite{haidar2015batched}. However, the importance of the batched GEMM goes beyond the boundaries 
of dense linear algebra to affect other scientific domains, such as sparse direct solvers~\cite{yeralan2017algorithm}, tensor 
contractions~\cite{jhurani2013gemm}~\cite{abdelfattah2016high}, and machine learning~\cite{chetlur2014cudnn}. 
The challenges in optimizing batched GEMM are different from the regular GEMM kernel. As the problem sizes are relatively smaller, 
the GEMM operations are no longer compute-bound, and more attention should be paid to optimizing the memory traffic. 
Automatic performance tuning is even more important in batched routines, since it has been found that the performance is more sensitive 
to tuning parameters in small matrix problems~\cite{abdelfattah2016performance}. 
%As an example, tensor contraction computations for large scale high-order FEM simulations can be also reduced 
%to batched GEMMs. 

Batched GEMM operations are crucial to machine learning applications in particular. For example, convolutional neural networks (CNNs) are a very popular 
class of deep neural networks (DNNs). They were initially implemented using custom dense kernels, as originally done in Caffe~\cite{jia2014caffe} and 
other libraries, such as tensor convolutions and activation functions. These custom kernels were developed locally per package. 
And since they dominate the training time for CNNs, re-optimizations had to be done whenever the underlying architecture changed. 
This is why research efforts, such as cuDNN~\cite{chetlur2014cudnn}, MagmaDNN~\cite{nwhtd:5:2018}, and others, focused on providing optimized primitives for deep learning, similar to 
the way BLAS provides optimized primitives to LAPACK algorithms. The most important operation in CNNs is batched spatial convolution, 
which can be cast into batched matrix multiplication~\cite{chellapilla2006high}~\cite{chetlur2014cudnn}. In addition, the work done in~\cite{lavin2016fast} 
uses batched GEMMs of very small sizes ($3\times 3$) to implement fast CNN algorithms based on minimal filtering algorithms~\cite{winograd1980arithmetic}. 
On another front, the batched GEMM operations in machine learning are not necessarily required to have the accuracy 
of single or double precisions. In fact, it has been shown that lower precisions are enough for training deep neural 
networks~\cite{gupta2015deep}. Furthermore, the need for extreme computational power in DNNs arises from their hyperparameter 
tuning---a process of training multiple DNNs to empirically find the best network in various 
applications~\cite{DBLP:journals/corr/abs-1808-07168}.
%
With the popularity of GPUs in large-scale AI applications, the latest architectures 
from NVIDIA, namely Volta and Turing, are equipped with Tensor Cores, which provide hardware acceleration for matrix-multiply-accumulate 
operations. The cuBLAS library provides high-level APIs for GEMM and batched GEMM in half precision (i.e., HGEMM and batched HGEMM, 
respectively). There are also low-level APIs that can be used to program the Tensor Cores inside a GPU kernel. 
While the high-level APIs have been used to accelerate mixed-precision iterative refinement dense linear 
solvers~\cite{haidar2018design,haidar2018harnessing}, this is the first effort, to the best of the authors' knowledge, to programmatically 
use the Tensor Cores in an open-source and general-purpose batched GEMM routine that is competitive with the vendor optimized library. 
%========================================================================================


