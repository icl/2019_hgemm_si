Comments from the editors and reviewers:
-Reviewer 1

  -

The core work presented in this paper is a highly tuned tensor-core based library for GEMM
and other computation. While I have minimal concern about the parts of manuscript that are
similar to the  IPDPS work, the newer work in this extension could be improved. The
authors state 2 new contributions: a theoretical analysis of planar vs interleaved layout
and experiment results for the same. I found the theoretical analysis confusing and hard
to follow. I would suggest the authors to explain how the split-execution based HCGEMM
would work, and then derive the results of theoretical analysis in a detailed manner. This
would include explaining how each of the flops and memory pressure terms are derived.
Experimental gains are notable, but seems to be derived from use of a different layout. It
will be good to separate out the gains due to magma’s tuning/implementation.



Other comments:

Section 6: what is the relationship of the third  reason  with inefficiency of planar
layout?

Fig 13 isn’t introduced, but directly used  in an analysis.


-Reviewer 2

  -
This paper proposes the use of tensor cores for performing half precision batch matrix
matrix multiplication computation using real and complex arithmetic using the tensor cores
on the GPU. The basic kernel design has already been reported in a paper at IPDPS 2019 but
the authors have generalized the design to address complex matrix matrix multiplication. 

The technique used by the authors to switch from interleaved format to split-complex
format is not new. It should be noted that many libraries out there already performed such
"de-interleaving" as part of their implementation. However, this is the first paper, to
the best of my knowledge, that performs the computation in the context of using the tensor
core on the Volta GPU.

While the analysis in Section 6 is appropriate, the assumptions are not stated clearly.
The assumption that HGEMM is applied on the entire input matrix when performing analysis
of the split format should be emphasized. By performing blocking and using the memory
hierarchy carefully, the analysis would not be as clear as to whether interleaved or split
format is better. An example of this is that the authors are using the split format
computation within their kernel and got their results because they are leveraging the
higher levels of the memory hierarchy when they perform blocking.

The roofline model is an architecture model, i.e. one per architecture. Therefore, the
theoretical bounds should be plotted on Figures 8 and 9 to provide context to the
achievable peak in comparison to the roofline model.

In terms of results, the experiments to demonstrate performance across different batch
sizes are noticeably missing from a paper on batched matrix multiplication. Currently,
only large batches are used (1k and 10k). At the minimum, explanation of why these are
appropriate sizes is expected. 
